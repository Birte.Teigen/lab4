package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int columns;

    CellState[][] grid = new CellState[numRows()][numColumns()];


    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;

        // rows and columns can not be a negative number, initialState can only be "DEAD" or "ALIVE"

        if (rows < 0 || columns < 0){
            throw new IllegalArgumentException("Rows and columns should not be negative or zero");
        }

        if (initialState.equals(CellState.DEAD)  || initialState.equals(CellState.ALIVE)){
        } else {
            throw new IllegalArgumentException("initialState should be DEAD or ALIVE");
        }

        // initialize grid
        this.grid = new CellState[rows][columns];
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        CellState cellState = grid[row][column];
        return cellState;
    }
   
    @Override
    public IGrid copy() {
        IGrid newGrid = new CellGrid(rows, columns, CellState.DEAD);
        
        for (int i = 0; i < newGrid.numRows(); i++) {
            for (int j = 0; j < newGrid.numColumns(); j++) {
                CellState newState = this.get(i, j);
                newGrid.set(i, j, newState);
            }
        }
        return newGrid;
    }
    
} 
